#include "cheby.h"
#include "fpminimax.h"
#include "minimax.h"
#include "plotting.h"
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <gmpxx.h>
#include <iostream>
#include <limits>

extern "C" {
#include <qsopt_ex/QSopt_ex.h>
}

using mpfr::mpreal;

void example_gen_atan(mp_prec_t prec) {
  std::function<mpreal(mpreal)> f = [](mpreal x) -> mpreal {
    return x > 0 ? atan(x) / x : mpreal(1);
  };
  std::function<mpreal(mpreal)> w = [f](mpreal x) -> mpreal {
    return mpreal(1) / f(x);
  };

  // domain
  auto dom = std::make_pair(mpreal(0.0), mpreal(1.0));
  // type
  std::pair<size_t, size_t> type = std::make_pair(29u, 29u);

  std::vector<mpfr::mpreal> num;
  std::vector<mpfr::mpreal> den;
  mpfr::mpreal delta;

  // prepare data for minimax algorithm execution
  std::vector<mpfr::mpreal> x(type.first + type.second + 2u);
  chebpts(x, x.size());
  chgvar(x, x, dom);

  std::vector<std::function<mpfr::mpreal(mpfr::mpreal)>> nbasis;
  std::vector<std::function<mpfr::mpreal(mpfr::mpreal)>> dbasis;
  nbasis.resize(type.first + 1u);
  dbasis.resize(type.second + 1u);
  for (int i{0}; i <= type.first; ++i)
    nbasis[i] = [i](mpreal x) -> mpreal { return mpfr::pow(x, i); };
  for (int i{0}; i <= type.second; ++i)
    dbasis[i] = [i](mpreal x) -> mpreal { return mpfr::pow(x, i); };

  auto success = minimax(delta, num, den, dom, f, w, type, true, prec);

  // prepare data for fpminimax algorithm execution
  std::vector<mp_prec_t> numPrec, denPrec;
  std::vector<mp_exp_t> numExp, denExp;
  std::vector<mpreal> fpnum, fpden;
  mp_prec_t man_size = 192ul;
  for (size_t i{0u}; i < nbasis.size(); ++i)
    numPrec.push_back(man_size);
  for (size_t i{0u}; i < dbasis.size(); ++i)
    denPrec.push_back(man_size);

  std::function<mpreal(mpreal)> p, q, err;
  construct_approx(p, q, err, nbasis, dbasis, num, den, f, w);

  std::string mname = "minimax";
  std::string msollyaOut = "atan_minimax.sollya";
  std::cout << "minimax error  = ";
  generate_output(err, dom, num, den, mname, msollyaOut, prec);
  // the index of the normalizing coefficient needed for constructing
  // the CVP lattice problem solved inside fpminimax
  size_t idx{0u};
  for (size_t i{0u}; i < den.size(); ++i) {
    if (den[i] == 1 || den[i] == -1)
      idx = i;
  }

  std::vector<mpreal> rdnum, rdden;
  rdnum.resize(num.size());
  rdden.resize(den.size());

  for (size_t i{0u}; i < num.size(); ++i) {
    mpreal buff = num[i];
    buff.set_prec(numPrec[i], GMP_RNDN);
    rdnum[i] = buff;
  }

  for (size_t i{0u}; i < den.size(); ++i) {
    mpreal buff = den[i];
    buff.set_prec(denPrec[i], GMP_RNDN);
    rdden[i] = buff;
  }

  std::function<mpreal(mpreal)> pr, qr, errr;
  construct_approx(pr, qr, errr, nbasis, dbasis, rdnum, rdden, f, w);

  std::string rdName = "rounding";
  std::string rdsollyaOut = "atan_rounding.sollya";
  std::cout << "rounding error  = ";
  generate_output(errr, dom, rdnum, rdden, rdName, rdsollyaOut, prec);

  std::function<mpreal(mpreal)> r = [p, q](mpreal var) -> mpreal {
    return p(var) / q(var);
  };

  fpminimax(fpnum, fpden, r, w, nbasis, dbasis, num, den, numPrec, denPrec, dom,
            idx, prec);

  // compute error of the fpminimax-type result
  std::function<mpreal(mpreal)> pfp, qfp, errfp;
  construct_approx(pfp, qfp, errfp, nbasis, dbasis, fpnum, fpden, f, w);

  std::string fpName = "fpminimax";
  std::string fpsollyaOut = "atan_fpminimax.sollya";
  std::cout << "fpminimax error = ";
  generate_output(errfp, dom, fpnum, fpden, fpName, fpsollyaOut, prec);
}

int main(int argc, char *argv[]) {
  mp_prec_t prec = 1024;
  mpreal::set_default_prec(prec);
  QSexactStart();
  mpf_QSset_precision(prec);

  example_gen_atan(prec);

  QSexactClear();
}